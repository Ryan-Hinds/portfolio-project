//Coding Challenge #01
/*
Calculate BMI using the following formula:
MBI = mass / height ** 2  or BMI = mass / (height * height)

tasks:
    Store Mark and John's mass & height in variables.
    Calc both their BMI using either formula (or both to see difference)
    Create a boolean variable "markHigherBMI" containing info about whether or not Mark has a higher BMI than John.

Test Data:
    Data 1: Mark weighs 78kg and is 1.69m tall, John weighs 92kg and is 1.95m tall
    Data 2: Mark weighs 95kg and is 1.88m tall, John is 85kg and 1.76m tall.
*/

// Data set 1
let markWeight = 78;
let markHeight = 1.69;
const markBMI = markWeight / markHeight ** 2;
console.log(`Mark's BMI is ${markBMI}.`);

let johnWeight = 92;
let johnHeight = 1.95;
const johnBMI = johnWeight / (johnHeight * johnHeight);
console.log(`John's BMI is ${johnBMI}.`);

let markHigherBMI = false;

// Check if Mark has a higher BMI (data set 1)
if (markBMI > johnBMI)
{
    markHigherBMI = true;
    console.log(`Mark's BMI is higher at ${markBMI} than John's BMI at ${johnBMI}.`);
    markHigherBMI = false;
}
else
{
    console.log(`John's BMI is higher at ${johnBMI} than Mark's BMI at ${markBMI}.`);
}

//Data set 2
markWeight = 95;
markHeight = 1.88;

johnWeight = 85;
johnHeight = 1.88;

if (markBMI > johnBMI)
{
    markHigherBMI = true;
    console.log(`Mark's BMI is higher at ${markBMI} than John's BMI at ${johnBMI}.`);
}
else
{
    console.log(`John's BMI is higher at ${johnBMI} than Mark's BMI at ${markBMI}.`);
}
