const firstName = "Ryan";
let job = "Programmer";
const birthYear = 1989;
let year = 2022;

const greeting = "I'm " + firstName + " and, I'm " + (year - birthYear) + " year old " + job + ".";
console.log(greeting);

// use `` instead of "" or '' for string interpolation. 
let newGreeting = `I'm ${firstName}, and I am a ${(year - birthYear)} year old ${job}.`;
console.log(newGreeting);
